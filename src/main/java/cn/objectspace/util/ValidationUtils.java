package cn.objectspace.util;

import java.util.regex.Pattern;

/**
 * @author pc
 */
public class ValidationUtils {

    private static final String PHONE_NUMBER_PATTERN = "^1[0-9]{10}$";
    private static final String EMAIL_PATTERN = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$";

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return Pattern.matches(PHONE_NUMBER_PATTERN, phoneNumber);
    }

    public static boolean isValidEmail(String email) {
        return Pattern.matches(EMAIL_PATTERN, email);
    }

    public static boolean isLengthValid(String str) {
        int minLength = 6;
        int maxLength = 18;
        int length = str.length();
        return length >= minLength && length <= maxLength;
    }
}