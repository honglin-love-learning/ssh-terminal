package cn.objectspace.util;

public class OperateString {
    public static Boolean isEditCommand(String originalString){
        String trim = originalString.trim();
        int spaceIndex = trim.indexOf(" ");

        if (spaceIndex != -1) {
            // 截取第一个空格之前的子字符串
            String result = trim.substring(0, spaceIndex);
            if(result.equals("vi")|result.equals("nano")|result.equals("vim ")|result.equals("emacs")|result.equals("gedit")){
                return true;
            }
            else {
                return false;
            }
        } else {
            // 如果没有空格，则原字符串保持不变
            return true;
        }

    }
}
