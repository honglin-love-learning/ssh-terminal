package cn.objectspace.util;

/**
 * @author yuwochangzai
 */
public class SshCodeMsg extends CodeMsg {

    private SshCodeMsg(Integer code, String msg)
    {
        super(code,msg);
    }

    public static final SshCodeMsg PHONE_ERROR = new SshCodeMsg(2001,"手机号格式错误");
    public static final SshCodeMsg EMAIL_ERROR = new SshCodeMsg(2002,"邮件格式错误");
    public static final SshCodeMsg LOGIN_USERNAME_ERROR = new SshCodeMsg(2003,"用户名错误或不存在");
    public static final SshCodeMsg LOGIN_PASSWORD_ERROR = new SshCodeMsg(2004,"密码错误");

    public static final SshCodeMsg LOGIN_ERROR = new SshCodeMsg(2005,"登录失败");
    public static final SshCodeMsg REGISTER_ERROR = new SshCodeMsg(2006,"注册失败");

    public static final SshCodeMsg REGISTER_ERROR_USERNAME = new SshCodeMsg(2006,"用户名已注册");

    public static final SshCodeMsg REGISTER_ERROR_LENGTH = new SshCodeMsg(2007,"请保证用户名密码长度为6-18");

    public static final SshCodeMsg LOGIN_TIMEOUT = new SshCodeMsg(2008,"登录失效");

    public static final SshCodeMsg LOGIN_PERMISSION_ERROR = new SshCodeMsg(2009,"没有权限");
    public static final SshCodeMsg USERNAME_PASSWORD_NULL = new SshCodeMsg(2010,"请输入用户名和密码");

    public static final SshCodeMsg USERNAME_NULL = new SshCodeMsg(2011,"请输入用户名");

    public static final SshCodeMsg PASSWORD_NULL = new SshCodeMsg(2011,"请输入密码");










}
