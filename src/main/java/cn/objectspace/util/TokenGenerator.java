package cn.objectspace.util;

import java.util.UUID;

/**
 * @author pc
 */
public class TokenGenerator {

    public static String generateToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}