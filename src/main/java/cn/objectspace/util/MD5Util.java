package cn.objectspace.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author pc
 */
public class MD5Util {
    public static String getMD5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] passwordBytes = password.getBytes();
            byte[] digest = md.digest(passwordBytes);
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String password = "your_password";
        String encryptedPassword = getMD5(password);
        System.out.println(encryptedPassword);
    }
}