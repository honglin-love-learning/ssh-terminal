package cn.objectspace.webssh.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

@Data
@TableName("ssh_connect")
public class SshConnect {

    private Long connectId;
    private Long userId;
    @TableField(value = "connect_name", exist = true)
    private String connectName = "新建会话";
    private String connectIp;
    private Integer connectPort;
    @TableField(value = "connect_user", exist = true)
    private String connectUser = "root";
    private String connectPassword;

    public Long getConnectId() {
        return connectId;
    }

    public void setConnectId(Long connectId) {
        this.connectId = connectId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getConnectName() {
        return connectName;
    }

    public void setConnectName(String connectName) {
        this.connectName = connectName;
    }

    public String getConnectIp() {
        return connectIp;
    }

    public void setConnectIp(String connectIp) {
        this.connectIp = connectIp;
    }

    public Integer getConnectPort() {
        return connectPort;
    }

    public void setConnectPort(Integer connectPort) {
        this.connectPort = connectPort;
    }

    public String getConnectUser() {
        return connectUser;
    }

    public void setConnectUser(String connectUser) {
        this.connectUser = connectUser;
    }

    public String getConnectPassword() {
        return connectPassword;
    }

    public void setConnectPassword(String connectPassword) {
        this.connectPassword = connectPassword;
    }
}