package cn.objectspace.filessh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SSHConnectUser {
    private String host;
    private int port;
    private String username;
    private String password;
}
