package cn.objectspace.filessh.user.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author pc
 */
@Data
@TableName("ssh_user")
public class SshUser {

    private Long userId;
    private String username;
    private String password;
    private String phone;
    private String email;
    private int role;


}