package cn.objectspace.filessh.vo;

import lombok.Data;

/**
 * @author pc
 */

@Data
public class SshUserVo {
    private String username;
    private String password;
}
