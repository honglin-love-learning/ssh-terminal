package cn.objectspace.filessh.interceptorconfig;

import cn.objectspace.filessh.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Autowired
    private  TokenInterceptor tokenInterceptor;

    @Autowired
    public InterceptorConfig(TokenInterceptor tokenInterceptor) {
        this.tokenInterceptor = tokenInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册拦截器
        registry.addInterceptor(tokenInterceptor)
                // 拦截所有路径
                .addPathPatterns("/**")
                // 排除/login路径和注册路径
                .excludePathPatterns("/user/login","/user/register","/user/admin/login");
    }
}