package cn.objectspace.filessh.service;

import cn.objectspace.filessh.user.pojo.SshUser;
import cn.objectspace.filessh.vo.SshUserVo;
import cn.objectspace.webssh.pojo.SshConnect;

import java.util.List;

/**
 * @author pc
 */
public interface IUserService {
    public boolean register(SshUser user);
    public String login(SshUserVo user);

    public String adminLogin(SshUserVo user);


    public List<SshConnect> getConnectMsg(String token);
}
