package cn.objectspace.filessh.exception;



import cn.objectspace.util.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 统一异常处理
 * @author yuwochangzai
 */
@Component
@RestControllerAdvice
public class CommonControllerAdvice {
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public Result handleBusinessException(BusinessException ex){
        System.out.println("123");
        return Result.error(ex.getCodeMsg());
    }
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String handleDefaultException(Exception ex){
        ex.printStackTrace();
        return Result.ERROR_MESSAGE;
    }
}
