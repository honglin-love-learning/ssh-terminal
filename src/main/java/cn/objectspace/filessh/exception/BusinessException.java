package cn.objectspace.filessh.exception;





import cn.objectspace.util.CodeMsg;
import cn.objectspace.util.SshCodeMsg;
import lombok.Getter;
import lombok.Setter;


/**
 * @author yuwochangzai
 */
@Setter
@Getter
public class BusinessException extends RuntimeException {
    private CodeMsg codeMsg;
    public BusinessException(SshCodeMsg codeMsg){
        this.codeMsg = codeMsg;
    }
}
