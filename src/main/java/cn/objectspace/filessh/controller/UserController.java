package cn.objectspace.filessh.controller;

import cn.objectspace.filessh.service.IUserService;
import cn.objectspace.filessh.user.pojo.SshUser;
import cn.objectspace.filessh.vo.SshUserVo;
import cn.objectspace.util.Result;
import cn.objectspace.util.SshCodeMsg;
import cn.objectspace.webssh.pojo.SshConnect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author pc
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    IUserService userService;

    @PostMapping("login")
    public Result<String> login (@RequestBody SshUserVo user){


        String loginToken = userService.login(user);

        return Result.success(loginToken);

    }

    @PostMapping("admin/login")
    public Result<String> adminLogin (@RequestBody SshUserVo user){


        String loginToken = userService.adminLogin(user);

        return Result.success(loginToken);

    }

    @PostMapping("/register")
    public Result<String> register (@RequestBody SshUser user){

        //保存到数据库中去
        boolean register = userService.register(user);
        if (register)
        {
            return Result.success("注册成功");
        }else {
            return  Result.error(SshCodeMsg.REGISTER_ERROR);
        }

    }

    @GetMapping("/getConnectMsg")
    public Result<List<SshConnect>> getConnectMsg(HttpServletRequest request){
        String token = request.getHeader("token");

        List<SshConnect> sshConnects =userService.getConnectMsg(token);

        return Result.success(sshConnects);
    }

    @GetMapping("/test")
    public Result<String> test(HttpServletRequest request){

        return Result.success("123");
    }


}
