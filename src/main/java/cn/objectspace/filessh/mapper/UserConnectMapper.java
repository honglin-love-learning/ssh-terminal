package cn.objectspace.filessh.mapper;

import cn.objectspace.webssh.pojo.SshConnect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pc
 */
@Mapper
public interface UserConnectMapper extends BaseMapper<SshConnect> {
}
