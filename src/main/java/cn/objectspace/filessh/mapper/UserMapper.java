package cn.objectspace.filessh.mapper;

import cn.objectspace.filessh.user.pojo.SshUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author pc
 */
@Mapper
public interface UserMapper extends BaseMapper<SshUser> {
}
